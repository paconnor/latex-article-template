# Proceedings

## Compilation

Clean output (anyway ignored by Git):
```
make clean
```

Just a quick test without figures or output:
```
make test
```

Just compile:
```
make compile
```

Just view:
```
make view
```
(Tuned to work both on Mac OS X and Linux.)

And to do everything:
```
make
```

## Differences

```
make diff
```
If work such that:
 - if you have commited the last changes, it will make the difference w.r.t. the last commit by another person;
 - if someone else commited the last changes, it will make the difference w.r.t. your last commit.

It uses `git-latexdiff`, obtained from the official [repo](https://gitlab.com/git-latexdiff/git-latexdiff).

