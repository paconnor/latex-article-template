DIR=output
CMD=pdflatex -output-directory $(DIR)
UNAME := $(shell uname)
VIEWER := evince

export max_print_line=1000

all: clean compile view

compile: output
	$(CMD) main.tex
	$(CMD) main.tex
	biber --input-directory $(DIR) --output-directory $(DIR) main
	$(CMD) main.tex

test: output
	$(CMD) -draftmode main.tex

DIFFCOMMIT=$(shell ./.diffcommit)

diff: output
	./.git-latexdiff "$(DIFFCOMMIT)" \
		--main main.tex \
		--ignore-makefile \
		--ignore-latex-errors \
		--output $(DIR)/diff.pdf \
		--view --pdf-viewer $(VIEWER) \
		--exclude-textcmd "section,subsection,subsubsection" \
		-c "PICTUREENV=(?:picture|tikzpicture|DIFnomarkup)[\w\d*@]*" 

output:
	@mkdir $(DIR)

view:
	$(VIEWER) $(DIR)/main.pdf

clean:
	@rm -rf $(DIR)

.PHONY: clean
